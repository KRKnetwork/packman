# Packman

Transpiler / Packager for .vue and .pug

## Usage

```javascript
const Packman = require('@krknet/packman')

const packman = new Packman({
  outputPath: path.join(global.__dataFolder, 'renders'),
  baseDir: __dirname
})

```

## Options

```javascript
{
  outputPath, // Output-Folder
  baseDir, // pug-BaseDir
  doCompress, // closure-compile if not dev
  localization, // Opional Localization Replacement $(VAR)$
  aliases, // Custom Import mapping [{from: '@', to: 'helpers/'}]
  env // development
}
```

### env
- packman: Babel Targets
- packman_compress=no: deactivate Minification in Productions

## Functions

```javascript
async cleanFolder () // Clean Ouput Folder

async packVueComponent ({ title, inputFile, toFile = true })  // iife -> path.join(this.outputPath, `component-${title}.js`)

async packVueApp ({ title, inputFile, toFile = true, outputModules = false }) // iife -> path.join(this.outputPath, `app-${title}.js`)

renderPug ({ title, templatePath, vars = {} })  // html -> path.join(this.outputPath, `${title}.html`)

stringPug ({ templatePath, vars = {} }) // outputs html

compilePug ({ templatePath, basevars = {} }) // outpurs stringFunction(locals)
```
