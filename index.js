/* eslint-disable security/detect-non-literal-fs-filename */
const pug = require('pug')
const fs = require('fs')
const path = require('path')
const rimraf = require('rimraf')
const { rollup } = require('rollup')
const pluginVue = require('rollup-plugin-vue')
const pluginCommon = require('@rollup/plugin-commonjs')
const pluginJSON = require('@rollup/plugin-json')
const pluginResolve = require('@rollup/plugin-node-resolve').default
const pluginAlias = require('@rollup/plugin-alias')
const pluginBabel = require('@rollup/plugin-babel').default
const pluginVirtual = require('@rollup/plugin-virtual')
const { terser } = require('rollup-plugin-terser')

module.exports = class Packman {
  constructor (options = {}) {
    if (!options.baseDir) throw new Error('Option "baseDir" needs to be defined!')

    this.outputPath = options.outputPath
    this.baseDir = options.baseDir
    this.localization = options.localization || false
    this.aliases = options.aliases || []
    this.env = options.env || process.env.NODE_ENV || 'development'
    this.downgrade = options.downgrade || process.env['packman']
    this.doCompress = this.env !== 'development' && (options.doCompress === undefined ? process.env['packman_compress'] !== 'no' : options.doCompress)

    if (this.outputPath && !fs.existsSync(this.outputPath)) fs.mkdirSync(this.outputPath, { recursive: true })
  }

  cleanFolder () {
    if (!this.outputPath) return

    return new Promise((resolve, reject) => {
      fs.access(this.outputPath, fs.constants.F_OK, err => {
        if (!err) {
          rimraf(this.outputPath, {}, () => {
            fs.mkdir(this.outputPath, { recursive: true }, err => err ? reject(err) : resolve())
          })
        } else {
          fs.mkdir(this.outputPath, { recursive: true }, err => err ? reject(err) : resolve())
        }
      })
    })
  }

  applyLocalization (input) {
    if (!this.localization) return input

    const matcher = new RegExp(/\$\((.+?)\)\$/, 'g')
    const output = input.replace(matcher, (reg, name) => this.localization.variables[name] || name)

    return output
  }

  renderPug ({ title, templatePath, vars = {} }) {
    if (!this.outputPath) throw new Error('Option "outputPath" needs to be defined!')

    const rawOutput = this.stringPug({ templatePath, vars })
    const staticPath = path.join(this.outputPath, `${title}.html`)
    fs.writeFileSync(staticPath, rawOutput)

    return staticPath
  }

  stringPug ({ templatePath, vars = {} }) {
    const rawOutput = pug.renderFile(templatePath, { basedir: this.baseDir, ...vars, __env: this.env })
    return this.applyLocalization(rawOutput)
  }

  compilePug ({ templatePath, basevars = {} }) {
    const stringFunction = pug.compileFile(templatePath, { basedir: this.baseDir })
    return locals => this.applyLocalization(stringFunction({ ...basevars, __env: this.env, ...(locals || {}) }))
  }

  async packVueComponent ({ title, inputFile, toFile = true }) {
    if (!inputFile) throw new Error('Option "inputFile" needs to be defined!')
    title = title || inputFile.split('/').reverse()[0].replace('.vue', '')

    const componentCode = `
      import component from '${inputFile}'

      // install function executed by Vue.use()
      function install (Vue) {
        if (install.installed) return
        install.installed = true
        Vue.component('${title}', component)
      }

      // Create module definition for Vue.use()
      const plugin = { install }

      // To auto-install when vue is found
      let GlobalVue = null
      if (typeof window !== 'undefined') {
        GlobalVue = window.Vue
      } else if (typeof global !== 'undefined') {
        GlobalVue = global.Vue
      }
      if (GlobalVue) GlobalVue.use(plugin)
    `
    const { doCompress, aliases, downgrade, localization } = this
    const bundle = await rollup({
      input: 'entry',
      ...rollupInput(componentCode, { doCompress, aliases, downgrade, localization })
    })

    const { output } = await bundle.generate({ format: 'iife', name: toCamel(title) })
    const outputCode = output[0].code
    if (!toFile) return outputCode

    if (!this.outputPath) throw new Error('Option "outputPath" needs to be defined!')
    const outputFile = typeof toFile === 'string' ? toFile : path.join(this.outputPath, `component-${title}.js`)
    await fs.promises.writeFile(outputFile, outputCode)
    return outputFile
  }

  async packVueApp ({ title, inputFile, toFile = true, outputModules = false }) {
    if (!inputFile) throw new Error('Option "inputFile" needs to be defined!')
    title = title || inputFile.split('/').reverse()[0].replace('.vue', '')

    const componentCode = `
    // Import vue component
    import App from '${inputFile}'

    /* eslint-disable-next-line no-new */
    new window.Vue({ render: h => h(App) }).$mount('#app')
    `

    const { doCompress, aliases, downgrade, localization } = this
    const bundle = await rollup({
      input: 'entry',
      ...rollupInput(componentCode, { doCompress, aliases, downgrade, localization })
    })

    if (toFile) {
      if (!this.outputPath) throw new Error('Option "outputPath" needs to be defined!')
      const outputFile = typeof toFile === 'string' ? toFile : path.join(this.outputPath, `app-${title}.js`)
      await bundle.write({ format: 'iife', file: outputFile })
      return outputModules ? { modules: clearModules(output[0].modules), file: outputFile } : outputFile
    } else {
      const { output } = await bundle.generate({ format: 'iife', name: toCamel(title) })
      return outputModules ? { modules: clearModules(output[0].modules), code: output[0].code } : output[0].code
    }
  }
}

const rollupInput = (componentCode, { doCompress, aliases, downgrade, localization }) => ({
  onwarn ({ loc, frame, message }) {
    if (loc) {
      console.warn(`${loc.file} (${loc.line}:${loc.column}) ${message}`)
      if (frame) console.warn(frame)
    } else {
      console.warn(message)
    }
  },
  plugins: [
    pluginVirtual({ entry: componentCode }),
    aliases.length > 0 && pluginAlias({ entries: aliases.map(({ from, to }) => ({ find: from, replacement: to })), Extensions: ['js', 'vue'] }),
    pluginResolve({ preferBuiltins: true, browser: true, extensions: ['.mjs', '.js', '.json', '.node', '.vue'] }),
    pluginCommon({ transformMixedEsModules: true }),
    pluginVue({ css: true, template: { isProduction: true } }),
    pluginJSON(),
    pluginLocalization(localization),
    downgrade && pluginBabel({
      babelrc: false,
      // exclude: 'node_modules/**',
      exclude: [/\/core-js\//],
      compact: false,
      babelHelpers: 'bundled',
      presets: [
        [
          '@babel/preset-env',
          {
            modules: false,
            targets: downgrade,
            useBuiltIns: 'usage',
            corejs: '3.11'
          }
        ]
      ]
    }),
    doCompress && terser({ format: { comments: false } })
  ]
})

const toCamel = (s) => s.replace(/([-_][a-z])/ig, $1 => $1.toUpperCase().replace('-', '').replace('_', ''))

const clearModules = modules => [...new Set(Object.keys(modules).map(d => d.split('?').shift()))]

const pluginLocalization = localization => {
  const matcher = new RegExp(/\$\((.+?)\)\$/, 'g')
  const replacer = input => input.replace(matcher, (reg, name) => localization.variables[name] || name)

  return {
    name: 'localization',
    transform (code) {
      if (!localization) return null
      return replacer(code)
    }
  }
}
